﻿using UnityEngine;
using System.Collections;


namespace Ai
{
    using Ai.StateMachine;

    public abstract class BaseAi : MonoBehaviour
    { 
        public Transform myTransform;
        public Animator animator;
        [HideInInspector]  public СoteState coteState;
        [HideInInspector]  public MoveState moveState;
        [HideInInspector]  public IdleState idleState;
        [HideInInspector]  public FollowState fallowState;

        [SerializeField]   protected GameObject selectedSprite;
        protected float waitTime = 2f;

        public IEnemyState currentState
        {
            set
            {
                _currentState = value;
                _currentState.OnEnter();
            }
            get
            {
                return _currentState;
            }
        }
        protected IEnemyState _currentState;

        protected virtual void Awake()
        {
            coteState = new СoteState(this);
            moveState = new MoveState(this);
            idleState = new IdleState(this);
            fallowState = new FollowState(this);
            myTransform = GetComponent<Transform>();
            animator = myTransform.GetComponent<Animator>();
        }

        // Use this for initialization
        protected virtual void Start()
        {
            currentState = idleState;           
        }

        // Update is called once per frame
        protected virtual void Update()
        {
            currentState.UpdateState();
        }

        protected virtual void FixedUpdate()
        {
            currentState.FixedUpdate();
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            currentState.OnTriggerEnter2D(other);
        }

        public virtual void Feed() { }

        public virtual void AddToGroup(ShepherdBehaviour shepherd) { }

        public virtual void FreeGroup() { }
        public virtual void SelectGroup() { }
        public virtual void DeliveryToCote() { }

    }
}