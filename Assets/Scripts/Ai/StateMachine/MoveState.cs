﻿using UnityEngine;
using System.Collections;

namespace Ai.StateMachine
{
    public class MoveState : IEnemyState
    {
        private readonly BaseAi aiBehaviour;

        private float speed = 0.5f;
        private float minDistance = 0.2f;
        private Vector3 moveVector;
        private int moveValue = 5;

        public MoveState(BaseAi state)
        {
            aiBehaviour = state;
            
        }
        #region IEnemyState

        public void FixedUpdate()
        {
            aiBehaviour.myTransform.position = Vector3.MoveTowards(aiBehaviour.myTransform.position, moveVector, Time.deltaTime * speed);
            if (aiBehaviour.myTransform.position.x > moveVector.x)
            {
                aiBehaviour.animator.SetInteger("Scale", 1);
            }
            else
            {
                aiBehaviour.animator.SetInteger("Scale", -1);
            }

                float distance = Vector3.Distance(aiBehaviour.myTransform.position, moveVector);
            if (distance <= minDistance)
            {
                ToIdleState();
            }
        }

        public void OnEnter()
        {
            moveVector = FindRandomPosition();
            aiBehaviour.animator.SetBool("isMove", true);
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == Constans.CoteTrriger)
            {
                ToСoteState();
            }
            else if (other.tag == Constans.DogTrriger)
            {
                var shepherd = other.gameObject.GetComponent<ShepherdBehaviour>();
                if (shepherd != null)
                {
                    if (shepherd.AddSheep(aiBehaviour))
                    {
                        ToFallowState();
                    }
                }
            }
        }

        public void ToIdleState()
        {
            aiBehaviour.currentState = aiBehaviour.idleState;
        }

        public void ToIdleMoveState()
        {

        }

        public void ToСoteState()
        {
            aiBehaviour.currentState = aiBehaviour.coteState;
        }

        public void UpdateState()
        {

        }

        public void ToFallowState()
        {
            aiBehaviour.currentState = aiBehaviour.fallowState;
        }
        #endregion

        private Vector3 FindRandomPosition()
        {
            //TODO: добавить ограничение на выход за пределы карты(если они будут)
            Vector3 pos = aiBehaviour.myTransform.position;
            int randomX = Random.Range(-moveValue, moveValue);
            int randomY = Random.Range(-moveValue, moveValue);
            pos.x += randomX;
            pos.y += randomY;
            return pos;
        }

    }
}