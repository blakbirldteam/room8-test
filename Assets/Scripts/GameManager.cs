﻿using UnityEngine;
using System.Collections;
using System;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    [HideInInspector] public ShepherdBehaviour previousShepherd;
    [HideInInspector] public ShepherdBehaviour currShepherd;

    [SerializeField] private InputManager inputManager;

    public ObjectPool Bonuses = new ObjectPool();
    public ObjectPool Cows = new ObjectPool();
    public ObjectPool Dogs = new ObjectPool();

    public int CowsLenght;
    public int DogsLenght;

    private float randomPosition = 5f;
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            DestroyImmediate(this);
        }
    }

    // Use this for initialization
    void Start()
    {
        inputManager.OnClick += onClick;

        InitLevel();
    }

    private void onClick(Vector3 obj)
    {
        if (currShepherd == null)
            return;

        currShepherd.MoveTo(obj);     
    }

    public void InitLevel()
    {
        Bonuses.Initialize();
        Cows.Initialize();
        Dogs.Initialize();
        for (int i = 0; i < CowsLenght; i++)
        {
            var cow = Cows.GetObject();
            Vector3 pos = new Vector3();
            pos.x = UnityEngine.Random.Range(-randomPosition, randomPosition);
            pos.y = UnityEngine.Random.Range(-randomPosition, randomPosition);
            cow.transform.position = pos;
            cow.SetActive(true);
        }
        for (int i = 0; i < DogsLenght; i++)
        {
            var dog = Dogs.GetObject();
            Vector3 pos = new Vector3();
            pos.x = UnityEngine.Random.Range(-randomPosition, randomPosition);
            pos.y = UnityEngine.Random.Range(-randomPosition, randomPosition);
            dog.transform.position = pos;
            dog.SetActive(true);
        }

    }

    public void DeliveryFeeds(ShepherdBehaviour shepherd)
    {
       if(shepherd.Sheeps.Count > 5)
        {
            SpounBonus();
        } 
    }

    private void SpounBonus()
    {
        var bonus = Bonuses.GetObject();
        Vector3 pos = new Vector3();
        pos.x = UnityEngine.Random.Range(-randomPosition, randomPosition);
        pos.y = UnityEngine.Random.Range(-randomPosition, randomPosition);
        bonus.transform.position = pos;
        bonus.SetActive(true);
    }

    public void SelectShepherd(ShepherdBehaviour newShepherd)
    {
        if (currShepherd != null)
        {
            previousShepherd = currShepherd;
            previousShepherd.FreeGroup();
        }

        currShepherd = newShepherd;
        currShepherd.SelectGroupe();
    }

  
}
