﻿using UnityEngine;
using System.Collections;

namespace Ai
{
    public class CowBehaviour : BaseAi
    {

        public override void AddToGroup(ShepherdBehaviour shepherd)
        {
            fallowState.SetShepherd(shepherd);
            selectedSprite.SetActive(true);
        }

        public override void DeliveryToCote()
        {
            selectedSprite.SetActive(false);
            currentState.ToСoteState();
        }

        public override void FreeGroup()
        {
            selectedSprite.SetActive(false);
        }

        public override void SelectGroup()
        {
            selectedSprite.SetActive(true);
        }

        public override void Feed()
        {
            StartCoroutine("WaitTime");
        }

        //Корова "пасется" и по истечению корутины идет в произвольном направлении
        protected IEnumerator WaitTime()
        {
            yield return new WaitForSeconds(waitTime);
            currentState.ToIdleMoveState();
        }


    }
}