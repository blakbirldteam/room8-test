﻿using UnityEngine;
using System.Collections;
using System;

namespace Ai.StateMachine
{
    public class СoteState : IEnemyState
    {
        private readonly BaseAi aiBehaviour;

        public СoteState(BaseAi state)
        {
            aiBehaviour = state;
        }
        #region IEnemyState
        public void OnTriggerEnter2D(Collider2D other)
        {
          
        }

        public void FixedUpdate()
        {

        }

        public void OnEnter()
        {
            aiBehaviour.animator.SetBool("isMove", false);
        }

        public void ToIdleState()
        {
            aiBehaviour.currentState = aiBehaviour.idleState;
        }

        public void ToIdleMoveState()
        {
            aiBehaviour.currentState = aiBehaviour.moveState;
        }

        public void ToСoteState()
        {

        }

        public void UpdateState()
        {

        }

        public void ToFallowState()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}