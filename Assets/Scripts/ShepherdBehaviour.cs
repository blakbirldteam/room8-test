﻿using UnityEngine;
using System.Collections;
using Ai;
using System.Collections.Generic;

public abstract class ShepherdBehaviour : MonoBehaviour
{
    public List<BaseAi> Sheeps;
    public Transform myTransform;

    protected float speed = 1f;
    protected Vector3 movePosition;
    protected bool ifNeedToMove;
    private float minDistance = 0.4f;

    protected virtual void Awake()
    {
        myTransform = GetComponent<Transform>();
    }

    // Use this for initialization
    protected virtual void Start()
    {
        Sheeps = new List<BaseAi>();

    }

    public virtual void MoveTo(Vector3 position)
    {
        ifNeedToMove = true;
        movePosition = position;
        foreach (var item in Sheeps)
        {
            item.fallowState.MoveTo(movePosition);
        }
    }

    public virtual bool AddSheep(BaseAi sheep)
    {
        if (Sheeps.Contains(sheep))
            return false;

        sheep.AddToGroup(this);
        Sheeps.Add(sheep);
        return true;
    }

    public virtual void FreeGroup()
    {
        foreach (var item in Sheeps)
        {
            item.FreeGroup();
        }
    }

    public void SelectGroupe()
    {
        foreach (var item in Sheeps)
        {
            item.SelectGroup();
        }
    }


    protected virtual void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == Constans.CoteTrriger)
        {
            if (Sheeps.Count != 0)
            {
                GameManager.Instance.DeliveryFeeds(this);
                foreach (var item in Sheeps)
                {
                    item.DeliveryToCote();
                }
                Sheeps = new List<BaseAi>();
            }
        }
    }

    protected void OnMouseDown()
    {
        GameManager.Instance.SelectShepherd(this);
    }

    protected virtual void FixedUpdate()
    {
        if (ifNeedToMove)
        {
            myTransform.position = Vector3.MoveTowards(myTransform.position, movePosition, Time.deltaTime * speed);
            float distance = Vector3.Distance(myTransform.position, movePosition);
            if (distance <= minDistance)
            {
                ifNeedToMove = false;
            }
        }
    }
}
