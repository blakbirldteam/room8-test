﻿using UnityEngine;
using System.Collections;
using System;

public class InputManager : MonoBehaviour
{

    public Action <Vector3> OnClick;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (OnClick != null)
            {
                target.z = 0;
                OnClick(target);
            }
        }
    }
}
