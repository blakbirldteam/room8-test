﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ObjectPool  {

    public GameObject _poolObject;
    public Transform _parent;
    public uint _poolLenght = 5;
    protected List<GameObject> _poolList = new List<GameObject>();

    public void Initialize()
    {
        if (_poolObject == null)
        {
            return;
        }
        for (int i = 0; i < _poolLenght; ++i)
        {
            CreateObject();
        }
    }

    public GameObject GetObject()
    {

        for (int i = 0; i < _poolList.Count; ++i)
        {
            if (!_poolList[i].activeInHierarchy)
            {
                return _poolList[i];
            }
        }
        int index = _poolList.Count;
        if (_poolObject == null)
        {
            return null;
        }
        CreateObject();
        return _poolList[index];
    }

    public void DestroyPool()
    {
        for (int i = _poolList.Count - 1; i >= 0; --i)
        {
            GameObject.Destroy(_poolList[i]);
        }
        _poolList.Clear();
    }

    protected void CreateObject()
    {
        GameObject go = GameObject.Instantiate(_poolObject) as GameObject;
        go.SetActive(false);
        if (_parent != null)
        {
            go.transform.parent = _parent;
            go.transform.position = go.transform.parent.position;
        }
        _poolList.Add(go);
    }

}