﻿using UnityEngine;
using System.Collections;

namespace Ai.StateMachine
{
    public interface IEnemyState
    {
        void OnEnter();

        void FixedUpdate();

        void UpdateState();

        void OnTriggerEnter2D(Collider2D other);

        void ToIdleState();

        void ToСoteState();

        void ToIdleMoveState();

        void ToFallowState();

    }
}