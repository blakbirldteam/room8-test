﻿using UnityEngine;
using System.Collections;

namespace Ai.StateMachine
{
    public class FollowState : IEnemyState
    {
        private readonly BaseAi aiBehaviour;

        protected ShepherdBehaviour sheepherd;
        protected float speed = 0.7f;
        protected Vector3 movePosition;
        private float minDistance = 0.4f;

        public FollowState(BaseAi state)
        {
            aiBehaviour = state;
        }
        #region IEnemyState

       /// <summary>
       /// Двигаем обьект к цели, если цели нету то идем к пастуху
       /// </summary>
        public void FixedUpdate()
        {
            if (sheepherd == null)
                return;

            aiBehaviour.myTransform.position = Vector3.MoveTowards(aiBehaviour.myTransform.position, movePosition, Time.deltaTime * speed);
            if (aiBehaviour.myTransform.position.x > movePosition.x)
            {
                aiBehaviour.animator.SetInteger("Scale", 1);
            }
            else
            {
                aiBehaviour.animator.SetInteger("Scale", -1);
            }

            float distance = Vector3.Distance(aiBehaviour.myTransform.position, movePosition);
            if (distance <= minDistance)
            {
                movePosition = sheepherd.myTransform.position;
            }
        }

        public void OnEnter()
        {
            aiBehaviour.animator.SetBool("isMove", true);
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            //TODO: Не понятно что делать если корова зашла в загон без поводыря?
            /*
            if (other.tag == Constans.CoteTrriger)
            {
                ToСoteState();
            }*/
        }

        public void ToIdleState()
        {
            aiBehaviour.currentState = aiBehaviour.idleState;
        }

        public void ToIdleMoveState()
        {

        }

        public void ToСoteState()
        {
            aiBehaviour.currentState = aiBehaviour.coteState;
        }

        public void UpdateState()
        {

        }

        public void ToFallowState()
        {
           
        }
        #endregion
        public void SetShepherd(ShepherdBehaviour shepherd)
        {
            this.sheepherd = shepherd;
        }

        public void MoveTo(Vector3 position)
        {
            movePosition = position;
        }

    }
}