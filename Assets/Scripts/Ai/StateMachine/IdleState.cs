﻿using UnityEngine;
using System.Collections;
using System;

namespace Ai.StateMachine
{
    public class IdleState : IEnemyState
    {
        private readonly BaseAi aiBehaviour;


        public IdleState(BaseAi state)
        {
            aiBehaviour = state;
        }

        #region IEnemyState

        public void FixedUpdate()
        {

        }

        public void OnEnter()
        {
            aiBehaviour.Feed();
            aiBehaviour.animator.SetBool("isMove", false);
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == Constans.DogTrriger)
            {
                var shepherd = other.gameObject.GetComponent<ShepherdBehaviour>();
                if(shepherd != null)
                {
                    if(shepherd.AddSheep(aiBehaviour))
                    {
                        ToFallowState();
                    }
                }           
            }
        }

        public void ToIdleState()
        {

        }

        public void ToIdleMoveState()
        {
            aiBehaviour.currentState = aiBehaviour.moveState;
        }

        public void ToСoteState()
        {
            aiBehaviour.currentState = aiBehaviour.coteState;
        }

        public void UpdateState()
        {

        }

        public void ToFallowState()
        {
            aiBehaviour.currentState = aiBehaviour.fallowState;
        }
        #endregion
    }
}