﻿using UnityEngine;
using System.Collections;

public class BonusBehaviour : MonoBehaviour
{
    public float waitTime = 10f;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("WaitToHide");
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == Constans.DogTrriger)
        {
            Debug.Log("OnTriggerEnter2D: " + other.name);
            gameObject.SetActive(false);
            StopAllCoroutines();
        }
    }

    IEnumerator WaitToHide()
    {
        yield return new WaitForSeconds(waitTime);
        gameObject.SetActive(false);
    }

}
